// numbers.js
// Some numbery things.

export function randomIntInRange(min, max, rng = Math.random) {
    return min + Math.floor((max - min + 1) * rng());
}

export function randomElementFromArray(a, rng = Math.random) {
    return a[randomIntInRange(0, a.length - 1, rng)];
}

export function randomSign(rng = Math.random) {
    return randomIntInRange(0, 1, rng) ? -1 : 1;
}

export function randomFloatInRange(min, max, rng = Math.random) {
    return min + (max - min) * rng();
}

export function clamp(n, min, max) {
    if (n < min) {
        return min;
    } else {
        return Math.min(n, max);
    }
}
