// jsuerdefaults.js
// Object to manage loading and saving user settings from/to localStorage if available.
// Vaguely inspired by NSUserDefaults ;)

// namespace is a string you can use so that different games in the same domain can use the same keys without clobbering each other.
// defaultValues is an object containing key and value pairs of default values, like:
// { sound: true, numberOfBodies: 4 }
// If you don't have any default values, then just don't pass anything.
// coding is a boolean; if false, we'll encode and decode values with JSON; if true, we'll do it with JSCoder to preserve type information. Using JSCoder will activate a "strict type mode" where accessing values will require type information for those values, either read from a passed-in default value or provided explicitly in .get().
// typeHints is a dictionary of type annotations for keys to use when decoding data from localStorage, useful if the default has no meaningful default value but you want to provide type information for it for later.

// Values are cloned (encoded and decoded into a new object) upon entering and exiting this object, so mutating any object seen by this class should not by reference modify any other object.


import { JSKeyedArchiver, JSKeyedUnarchiver, getTypeOf, ArrayType } from "./jscoder.js";


export class JSUserDefaults {
    constructor(namespace = "", defaultValues = {}, coding = false, typeHints = {}) {
        // Check if we have localStorage
        this.localStorage = undefined;
        try {
            let localStorage = window.localStorage;
            localStorage.setItem("jsud_storagetest", "whattup");
            localStorage.removeItem("jsud_storagetest");
            this.localStorage = localStorage;
        } catch (e) {
            console.warn("localStorage isn't available, so user data won't be saved after the page is closed");
            this.makeDummyStorage();
        }

        this.keyPrefix = "jsud_";
        if (namespace != "") {
            this.keyPrefix += `${namespace}_`;
        }

        if (coding) {
            this.coder = new JSCoderCoder();
            this.getTypeOf = getTypeOf;
        } else {
            this.coder = new JSONCoder();
            this.getTypeOf = function(obj) { return undefined; }
        }

        this.defaultValues = {};
        this.typeHints = Object.assign({}, typeHints);
        for (let [key, value] of Object.entries(defaultValues)) {
            let clonedValue = this.coder.clone(value);
            this.defaultValues[key] = clonedValue;
            if (!this.typeHints.hasOwnProperty(key)) {
                this.typeHints[key] = this.getTypeOf(clonedValue);
            }
        }
        this.values = {};
    }

    // If a value with the given key has been saved through defaults, returns that value.
    // If no value with the given key has been saved but a default value was provided in the constructor, returns that value.
    // Otherwise, returns null.
    // The second argument is a type that can be provided to help JSUserDefaults in decoding data from localStorage, if you're in "strict type mode." If you've provided a default value for the key, we'll use the type of that value to decode the data on first access, but if you don't provide that because a default value doesn't make sense, then use the second argument to this function.
    get(key, typeHint) {
        if (this.values.hasOwnProperty(key)) {
            return this.coder.clone(this.values[key]);
        }

        if (this.keyExistsInStorage(key)) {
            let type = typeHint ?? this.typeHints[key]
            this.values[key] = this.getStoredValue(key, type);
            return this.coder.clone(this.values[key]);
        }

        if (this.defaultValues[key] !== undefined) {
            this.values[key] = this.defaultValues[key];
            return this.coder.clone(this.values[key]);
        }

        return null;
    }

    // Saves a key & value in localStorage.
    set(key, value) {
        let clonedValue = this.coder.clone(value);
        this.values[key] = clonedValue;
        this.storeValue(key, clonedValue);
    }

    // Removes a key and its associated value from localStorage.
    remove(key) {
        let storageKey = this.storageKeyFor(key);
        delete this.values[key];
        this.localStorage.removeItem(storageKey);
    }

    // Removes all stored values associated with the current namespace.
    removeAll() {
        // Remove all values we're holding onto from here and localStorage
        for (let key of Object.keys(this.values)) {
            this.remove(key);
        }

        // Go through everything that's left in localStorage and delete any data belonging to the current namespace
        let keys = [];
        for (let i = 0; i < this.localStorage.length; i++) {
            let key = this.localStorage.key(i);
            if (key.startsWith(this.keyPrefix)) {
                keys.push(key);
            }
        }
        for (let key of keys) {
            this.localStorage.removeItem(key);
        }
    }


    // private

    storageKeyFor(key) {
        return this.keyPrefix + key;
    }

    keyExistsInStorage(key) {
        return (this.localStorage.getItem(this.storageKeyFor(key)) !== null);
    }

    getStoredValue(key, type) {
        let storageKey = this.storageKeyFor(key);
        try {
            return this.coder.decode(this.localStorage.getItem(storageKey), type);
        } catch (e) {
            if (e instanceof TypeRequiredError) {
                throw new TypeError(`No type could be inferred for default key ${key}. Either provide a type hint for this key in the JSUserDefaults constructor or explicitly pass a type as the second argument to JSUSerDefaults.get().`);
            } else if (e instanceof EmptyArrayTypeHintError) {
                throw new TypeError(`Couldn't decode stored data for default key ${key} with an empty array type hint, which probably happened because an empty array was provided as a default value. Either provide a fully specified array type hint for this key in the JSUserDefault constructor or explicitly pass one as the second argument to JSUserDefaults.get().`);
            } else {
                throw e;
            }
        }
    }

    storeValue(key, value) {
        let storageKey = this.storageKeyFor(key);
        let encodedValue = this.coder.encode(value);

        try {
            this.localStorage.setItem(storageKey, encodedValue);
        } catch (e) {
            console.error(`There was some kind of error when trying to save ${key} to localStorage, which probably means we've hit the storage quota. No further data will be saved.`, value);
            this.makeDummyStorage();
        }
    }

    makeDummyStorage() {
        this.localStorage = {
            setItem: function() { },
            getItem: function() { return null; },
            removeItem: function() { },
            length: 0
        };
    }
}


class JSONCoder {
    encode(obj) {
        return JSON.stringify(obj);
    }

    decode(s, type) {
        return JSON.parse(s);
    }

    clone(obj) {
        return JSON.parse(JSON.stringify(obj));
    }
}

class JSCoderCoder {
    encode(obj) {
        return JSKeyedArchiver.archivedDataWithObject(obj);
    }

    decode(s, type) {
        if (type !== undefined) {
            try {
                return JSKeyedUnarchiver.unarchivedObjectOfTypeFromData(type, s);
            } catch (e) {
                if (type instanceof ArrayType && type.baseType === undefined) {
                    throw new EmptyArrayTypeHintError();
                }
            }
        } else {
            throw new TypeRequiredError();
        }
    }

    clone(obj) {
        let type = getTypeOf(obj) ?? "String";
        return this.decode(this.encode(obj), type);
    }
}

class TypeRequiredError extends Error { }
class EmptyArrayTypeHintError extends Error { }
