JS utilities
============

Random utility code I use in my JavaScript projects. Absolutely none of this is "production ready" so probably don't ever use this anywhere

All the code here is by me and released under MIT (see `LICENSE.txt`), except the numeric springing code in `numbers-with-springing.js`, written by Allen Chou and released under MIT (see license in that file).

`tests/fonts/` contains the font [Roboto by Christian Robertson](https://fonts.google.com/specimen/Roboto#about), released under the Apache License, Version 2.0 (see `LICENSE.txt` in that directory).
