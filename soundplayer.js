// soundplayer.js
// Object for simple playback of sound effects.

export class SoundPlayer {
    constructor(soundBuffers, context) {
        this.soundBuffers = soundBuffers;
        this.soundChannels = new Object();
        this.context = context;

        this.nextChannelID = 0;

        this.masterGain = this.context.createGain();
        this.setVolume(1);
        this.masterGain.connect(this.context.destination);

        this.masterCompressor = this.context.createDynamicsCompressor();
        this.masterCompressor.connect(this.masterGain);
    }

    addBuffers(newBuffers) {
        Object.assign(this.soundBuffers, newBuffers);
    }

    play(key, args = {}) {
        let channelID = this.nextChannelID;
        this.nextChannelID += 1;

        let delay = args.delay ?? 0;
        let offset = args.offset ?? 0;
        let loop = args.loop ?? false;
        let exclusive = args.exclusive ?? false;

        if (exclusive) {
            this.stop(key);
        }

        let sourceNode = this.context.createBufferSource(), gainNode = this.context.createGain();
        this.soundChannels[channelID] = {key: key, sourceNode: sourceNode, gainNode: gainNode};

        sourceNode.buffer = this.soundBuffers[key];
        if (loop) {
            sourceNode.loop = true;
        }
        sourceNode.onended = function() { this.stopChannelID(channelID); }.bind(this);
        sourceNode.connect(gainNode);
        gainNode.connect(this.masterCompressor);
        sourceNode.start(this.context.currentTime + delay, offset);
    }

    stop(key) {
        for (let id of Object.keys(this.soundChannels)) {
            if (this.soundChannels[id].key == key) {
                this.stopChannelID(id);
            }
        }
    }

    stopAllSounds() {
        for (let id of Object.keys(this.soundChannels)) {
            this.stopChannelID(id);
        }
    }

    fadeOut(key, fadeDuration) {
        for (let id of Object.keys(this.soundChannels)) {
            if (this.soundChannels[id].key == key) {
                this.fadeOutChannelID(id, fadeDuration);
            }
        }
    }

    setVolume(volume) {
        this.masterGain.gain.setValueAtTime(Math.min(volume * volume, 1), 0);
    }


    // private

    stopChannelID(id) {
        this.soundChannels[id].sourceNode.onended = undefined;
        this.soundChannels[id].sourceNode.stop();
        this.soundChannels[id].gainNode.disconnect();
        delete this.soundChannels[id];
    }

    fadeOutChannelID(id, fadeDuration) {
        this.soundChannels[id].gainNode.gain.setTargetAtTime(0, 0, fadeDuration / 4);
        window.setTimeout(function() {
            this.stopChannelID(id);
        }.bind(this), fadeDuration * 1000);
    }
}
