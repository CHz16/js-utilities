// musicplayer.js
// Object for playback of music.

const MUSIC_PLAYER_FADE_DURATION = 2; // seconds


export class MusicPlayer {
    constructor(musicData, context) {
        this.musicData = musicData;
        this.musicChannels = [];
        this.context = context;

        this.masterGainNode = this.context.createGain();
        this.masterGainNode.gain.setValueAtTime(1, 0);
        this.masterGainNode.connect(this.context.destination);

        // The music player needs an AnalyserNode for the waveform visualization, so we pass the music into that before sending it to the master gain for overall volume control.
        // However, an AnalyserNode can't combine multiple inputs, so we need to consolidate all the music channels into another GainNode first before passing it to the analyser.
        this.analyserNode = this.context.createAnalyser();
        this.analyserNode.fftSize = 2048;
        this.analyserNode.connect(this.masterGainNode);
        this.consolidatorNode = this.context.createGain();
        this.consolidatorNode.connect(this.analyserNode);

        this.cullTimeout = undefined;
    }

    addData(newData) {
        Object.assign(this.musicData, newData);
    }

    play(key, args = {}) {
        let loop = args.loop ?? true;
        let sync = args.sync ?? false;
        let fadeDuration = args.fadeDuration ?? MUSIC_PLAYER_FADE_DURATION;

        this.stop(fadeDuration);

        let sourceNode = this.context.createBufferSource();
        sourceNode.buffer = this.musicData[key].buffer;
        if (loop) {
            sourceNode.loop = true;
        }
        if (this.musicData[key].loopStart !== undefined) {
            sourceNode.loopStart = this.musicData[key].loopStart;
            sourceNode.loopEnd = this.musicData[key].buffer.duration;
        }

        let gainNode = this.context.createGain();
        gainNode.gain.setValueAtTime(0, 0);

        this.musicChannels.push({sourceNode: sourceNode, gainNode: gainNode, startTime: this.context.currentTime, key: key, fading: false});

        sourceNode.connect(gainNode);
        gainNode.connect(this.consolidatorNode);
        if (!sync || this.musicChannels.length == 1) {
            sourceNode.start();
        } else {
            // Sync with the previous channel, which is second from the end (since the last one is the channel we just made)
            let syncChannel = this.musicChannels[this.musicChannels.length - 2], syncData = this.musicData[syncChannel.key];
            this.musicChannels[this.musicChannels.length - 1].startTime = syncChannel.startTime;
            let currentPosition = this.channelPositionInfo(syncChannel).currentPosition;
            sourceNode.start(this.context.currentTime, currentPosition);
        }
        gainNode.gain.setTargetAtTime(1, 0, fadeDuration / 4);
    }

    stop(fadeDuration = MUSIC_PLAYER_FADE_DURATION) {
        let liveChannels = this.musicChannels.length;
        if (liveChannels == 0) {
            return;
        }

        if (this.cullTimeout !== undefined) {
            window.clearTimeout(this.cullTimeout);
        }

        for (let i = 0; i < liveChannels; i++) {
            this.musicChannels[i].gainNode.gain.setTargetAtTime(0, 0, fadeDuration / 4);
            this.musicChannels[i].fading = true;
        }

        this.cullTimeout = window.setTimeout(function() {
            for (let i = 0; i < liveChannels; i++) {
                let channel = this.musicChannels[i];
                channel.sourceNode.stop();
                channel.sourceNode.disconnect();
                channel.gainNode.disconnect();
            }
            this.musicChannels.splice(0, liveChannels);
            this.cullTimeout = undefined;
        }.bind(this), fadeDuration * 1000);
    }

    setVolume(volume) {
        this.masterGainNode.gain.setValueAtTime(Math.min(volume * volume, 1), 0);
    }

    currentSong() {
        let lastChannel = this.musicChannels[this.musicChannels.length - 1];
        if (lastChannel === undefined || lastChannel.fading) {
            return null;
        } else {
            return lastChannel.key;
        }
    }

    currentSongPositionInfo() {
        let lastChannel = this.musicChannels[this.musicChannels.length - 1];
        if (lastChannel === undefined || lastChannel.fading) {
            return null;
        } else {
            return this.channelPositionInfo(lastChannel);
        }
    }

    setPlaybackRateOfCurrentChannels(rate, time) {
        for (let i = 0; i < this.musicChannels.length; i++) {
            this.musicChannels[i].sourceNode.playbackRate.setTargetAtTime(rate, 0, time / 4);
        }
    }


    // private

    channelPositionInfo(channel) {
        let songData = this.musicData[channel.key];
        let currentPosition = this.context.currentTime - channel.startTime;
        if (songData.loopStart !== undefined && currentPosition > songData.loopStart) {
            currentPosition = (currentPosition - songData.loopStart) % (songData.buffer.duration - songData.loopStart) + songData.loopStart;
        } else {
            currentPosition = currentPosition % songData.buffer.duration;
        }
        return {currentPosition: currentPosition, duration: songData.buffer.duration};
    }
}
