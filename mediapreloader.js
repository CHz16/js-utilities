// mediapreloader.js
// Unified async preloader for loading images, audio, and fonts.

/*

Intended for use like this:
    PreloadMedia(mediaData, args)
    .then(media => doStuffWith(media))
    .catch(erroredData => console.error(erroredData));

mediaData is an array of filenames as strings or objects specifying media. If an element is a string, its type will be inferred from its extension. See sections below for specifics on object specifications for each type.

args is an optional argument containing one or more additional parameters. The possible type-independent parameters are:
    * maxRequests: an integer indicating the maximum number of open requests. Default is 10.
    * progressCallback: a function that will be called whenever something is loaded. It takes two parameters: data (an object specification for the thing that was just loaded) and numberLoaded (the number of things that have been loaded).

The argument to the success callback, media, contains four properties: images, sound, music, and fonts. Each of these is a dictionary containing all the loaded objects of that type, keyed to the "key" property of that object specification. If no key is provided for that object, one will be assigned based on that object's filename without the file extension.

The error callback is given an object containing two properties: exception, the exception object thrown by the loader, and data, the object specification of the thing that failed loading.


=Images=

Inferred file extensions: .jpg, .png, .gif

Object specification properties:
    * type (optional): must be "image"
    * url (required): the URL of the image to load
    * key (optional): the key to access the image with in media.images

Type-specific args:
    * imageBase (optional): a base directory to append to the start of all image URLs

media.images contents:
    * Just an Image object for the preloaded image


=Fonts=

Inferred file extensions: .ttf

Object specification properties:
    * type (optional): must be "font"
    * url (required): the URL of the font to load
    * key (optional): the key to access the font with in media.fonts
    * family (optional): the family name for the font. Defaults to the key
    * addToFonts (optional): whether to automatically add the font to document.fonts. Defaults to true

Type-specific args:
    * fontBase (optional): a base directory to append to the start of all font URLs

media.fonts contents:
    * Just an FontFace object for the preloaded font


=Sounds=

Inferred file extensions: .wav, .ogg, .mp3

Object specification properties:
    * type (optional): must be "sound"
    * urls (required): an array of URLs of the sound to try loading in order
        OR
      url (required): a single URL
    * key (optional): the key to access the sound with in media.sounds

Type-specific args:
    * audioContext (required): an audio context to create the buffers in
    * soundBase (optional): a base directory to append to the start of all sound URLs

media.sounds contents:
    * Just an AudioBuffer object for the preloaded sound. The entirety of media.sounds is suitable to give as the soundBuffers argument of a SoundPlayer.


=Music=

Inferred file extensions: none (all audio is inferred to be "sound")

Object specification properties:
    * type (required): must be "music"
    * urls (required): an array of URLs of the music to try loading in order
        OR
      url (required): a single URL
    * key (optional): the key to access the music with in media.music
    * loopStart (optional): time in seconds to restart from when looping. This isn't used at all by the preloader, but is instead passed through in media.music.

Type-specific args:
    * audioContext (required): an audio context to create the buffers in
    * musicBase (optional): a base directory to append to the start of all music URLs

media.music contents:
    * An object with a buffer property containing an AudioBuffer object. If loopStart was provided in the object specification, it will be present in the object as well. The entirety of media.music is suitable to give as the musicData argument of a MusicPlayer.

*/

const MIME_TYPE_FOR_EXTENSION = {
    "wav": "audio/wav",
    "mp3": "audio/mpeg",
    "ogg": "audio/ogg",
};
Object.freeze(MIME_TYPE_FOR_EXTENSION);

export function PreloadMedia(incomingMediaData, args = {}) {
    return new Promise(function(resolve, reject) {
        // Declarations

        let imageBase = args.imageBase ?? "";
        let soundBase = args.soundBase ?? "";
        let musicBase = args.musicBase ?? "";
        let fontBase = args.fontBase ?? "";
        let maxRequests = args.maxRequests ?? 10;
        let progressCallback = args.progressCallback ?? function(data, numberLoaded) { };

        let nextToLoad = 0, finishedLoading = 0;
        let died = false;

        let mediaData = [];
        let images = {}, sounds = {}, music = {}, fonts = {};
        let canPlayType = {};


        // Helper async function defs

        function preloadNext() {
            let i = nextToLoad;
            nextToLoad += 1;
            if (mediaData[i].type == "image") {
                let image = new Image();
                image.src = `${imageBase}${mediaData[i].url}`;
                image.decode().then(() => finishLoading(i, image)).catch((exception) => erroredLoading(i, exception));
            } else if (mediaData[i].type == "font") {
                let fontFace = new FontFace(mediaData[i].family, `url(${fontBase}${mediaData[i].url})`);
                fontFace.load().then(loadedFontFace => finishLoading(i, fontFace)).catch((exception) => erroredLoading(i, exception));
            } else if (mediaData[i].type == "sound" || mediaData[i].type == "music") {
                // Find the first sound file that the browser can play.
                let soundUrl, extension;
                for (let url of mediaData[i].urls) {
                    extension = url.substring(url.lastIndexOf(".") + 1);
                    if (canPlayType[extension]) {
                        soundUrl = url;
                        break;
                    }
                }

                // If the browser can't play any of them, then error.
                if (soundUrl === undefined) {
                    erroredLoading(i, new Error("No supported audio format found"));
                    return;
                }

                // Otherwise, kick off an AJAX request for the sound.
                let base = (mediaData[i].type == "sound") ? soundBase : musicBase;
                let request = new XMLHttpRequest();
                request.open("GET", `${base}${soundUrl}`);
                request.responseType = "arraybuffer";
                request.addEventListener("load", event => decodeAudio(i, event));
                request.addEventListener("error", event => erroredLoading(i, new Error("XMLHttpRequest failed, couldn't load sound")));
                request.send();
            } else {
                erroredLoading(i, new Error("No recognized type in object specification"));
            }
        }

        function decodeAudio(i, event) {
            args.audioContext.decodeAudioData(event.target.response)
            .then(buffer => finishLoading(i, buffer))
            .catch(exception => erroredLoading(i, exception));
        }

        function finishLoading(i, thing) {
            if (died) {
                return;
            }

            finishedLoading += 1;
            let numberLoaded = finishedLoading;

            if (mediaData[i].type == "image") {
                images[mediaData[i].key] = thing;
            } else if (mediaData[i].type == "font") {
                fonts[mediaData[i].key] = thing;
                if (mediaData[i].addToFonts) {
                    document.fonts.add(thing);
                }
            } else if (mediaData[i].type == "sound") {
                sounds[mediaData[i].key] = thing;
            } else if (mediaData[i].type == "music") {
                music[mediaData[i].key] = {buffer: thing};
                if (mediaData[i].loopStart !== undefined) {
                    music[mediaData[i].key].loopStart = mediaData[i].loopStart;
                }
            }

            progressCallback(mediaData[i], numberLoaded);

            if (nextToLoad < mediaData.length) {
                preloadNext();
            } else if (numberLoaded == mediaData.length) {
                resolve({ images: images, sounds: sounds, music: music, fonts: fonts});
            }
        }

        function erroredLoading(i, exception) {
            died = true;
            reject({exception: exception, data: mediaData[i]});
        }


        // Actual constructor operations

        // Check what audio formats the browser can and can't play.
        let audioTester = new Audio();
        for (let extension of Object.keys(MIME_TYPE_FOR_EXTENSION)) {
            let result = audioTester.canPlayType(MIME_TYPE_FOR_EXTENSION[extension]);
            canPlayType[extension] = (result !== "" && result !== "no");
        }

        // Preprocess the media data given to infer/fill in default fields
        for (let thing of incomingMediaData) {
            let data;
            if (typeof thing == "string") {
                data = { url: thing };
            } else if (thing.constructor.name == "Array") {
                data = { type: "sound", urls: thing };
            } else {
                data = Object.assign({}, thing);
            };

            let testUrl = (data.url !== undefined) ? data.url : data.urls[0];
            let lastPeriodIndex = testUrl.lastIndexOf(".");
            if (data.key === undefined) {
                data.key = testUrl.substring(0, lastPeriodIndex);
            }
            if (data.type === undefined) {
                let extension = testUrl.substring(lastPeriodIndex + 1).toLowerCase();
                if (extension == "jpg" || extension == "png" || extension == "gif") {
                    data.type = "image";
                } else if (extension == "ttf") {
                    data.type = "font";
                } else if (extension == "ogg" || extension == "mp3" || extension == "wav") {
                    data.type = "sound";
                }
            }

            if (data.type == "font") {
                data.addToFonts = data.addToFonts ?? true;
                data.family = data.family ?? data.key;
            } else if (data.type == "sound" || data.type == "music") {
                if (data.urls === undefined) {
                    data.urls = [data.url];
                    delete data.url;
                }
            }

            mediaData.push(data);
        }

        // Kick off loading
        // Each preloadNext will chain into exactly one finishLoading and back into at most one preloadNext, so we'll never have more than maxRequest chains going at the same time
        for (let i = 0; i < Math.min(mediaData.length, maxRequests); i++) {
            preloadNext();
        }
    });
}
