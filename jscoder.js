// jscoder.js
// Heavily inspired by NSCoder & NSCoding ;)

/*
This is a framework for serializing and unserializing "struct-y" JavaScript objects so that they don't lose their type information when you reconstruct them later. Serialized output is as a string, so this can be used to stash objects in localStorage and sessionStorage.

This is NOT a general library for serializing any JavaScript object you can possibly construct. The intended use case is to save preferences and model data for game state, so objects should be "struct-y" or "value type-y"—meaning that they should broadly be composed of types that are themselves codable and should own all their children (references are dangerous, circular references are extremely bad).

To encode or decode an object, use JSKeyedArchiver.archivedDataWithObject and JSKeyedUnarchiver.unarchivedObjectOfTypeFromData, like so:

let data = JSKeyedArchiver.archivedDataWithObject(sourceObject);
let newObject = JSKeyedUnarchiver.unarchivedObjectOfTypeFromData(SourceObjectType, data);

Classes must explicitly opt in to be codable in one of two ways. The manual way, which you may be familiar with if you've programmed for an Apple platform in the last 15 years, is for the class to implement two methods: encodeWith(coder) and static initWith(coder). Note that the first is an instance method and the second is a class method. In the body of encodeWith, you'll need to save all the properties you need to restore the object into the passed-in coder using coder.encode(object, key); anything you save into a coder must itself be codable, which is why this works best with classes that are composed entirely of other codable objects, though you can certainly convert an uncodable object into some codable representation on your own and then save that if you want to do that to yourself. In the body of initWith, you'll need to access all those objects you saved with coder.decode(type, key) and use them to construct a new object with those properties.

Here's an example implementation of these methods:

class Thing {
    constructor(string, number) {
        this.string = string;
        this.number = number;
    }

    encodeWith(coder) {
        coder.encode(this.string, "string");
        coder.encode(this.number, "number");
    }

    static initWith(coder) {
        return new Thing(coder.decode(String, "string"), coder.decode(Number, "number"));
    }
}

Notice that you're required to provide a type when decoding an object. Every type is considered an "optional," meaning that you can encode undefined or null and they'll decode as undefined or null no matter which type you provide to decode().

The other and much easier way to opt in to being codable is to have the framework synthesize the coding for you at runtime; signal this by defining static JSCODING_SYNTHESIZE_METHODS = true on your class. To encode, it'll iterate through all the properties set on the object and add them to the encoder. This means that all the object's properties must be codable; if you have to do any massaging of the property to make it codable, you'll have to write encodeWith yourself.

To decode, it'll instantiate a new object of the type and iterate through each property, decode that property from the decoder, and set the property to that decoded object. This means two things: the framework must be able to construct an object, and it has to be able to figure out the type of every property in the newly constructed object. If the class has a constructor that takes no arguments and every property has a default value that a type can be discerned from, then everything's fine, but if one or both of those things isn't true, you'll need to provide additional information.

If there's no way around the constructor requiring arguments, you can provide an array of arguments to pass into the constructor by defining static JSCODING_CONSTRUCTOR_ARGUMENTS = [...] on your class. If the synthesized decoder is unable to figure out the type of a property, for example because its default value is undefined, then you can provide type information for it by defining static JSCODING_TYPE_ANNOTATIONS = { propertyName: type } on your class.

Since the properties are inspected at runtime to make this work, you should probably make sure that all the properties the object will ever have are all set in the object's constructor, even just to undefined if they're not relevant immediately on object creation, so that the iteration can find them.
*/

const JSCODER_ROOT_OBJECT_KEY = "root";


export class JSKeyedArchiver {
    constructor() {
        this.record = {};
    }

    static archivedDataWithObject(obj) {
        let coder = new JSKeyedArchiver();
        coder.encode(obj, JSCODER_ROOT_OBJECT_KEY);
        return coder.encodedData();
    }

    encode(obj, key) {
        // Intercept undefined and null values and box them up into something we can serialize and unserialize
        try {
            obj = new BoxedNothing(obj);
        } catch {
            // Do nothing - just keep obj as it was passed in
        }

        // Recursive base case: we were given the raw data encoding of an object, so save that and leave
        if (obj.constructor.name === "JSCoderEncodedData") {
            this.record[key] = obj.data;
            return;
        }

        // Special case check if the object is a bare Object to show a special error message; could've done this by having Object implement the JSCoding methods, but then literally every object of any type would inherit those.
        if (obj.constructor.name === "Object") {
            throw new TypeError("JSKeyedArchiver error: bare Objects can't be encoded. If this is a struct-y object, consider declaring it as a class to provide type information for decoding.");
        }

        // Otherwise, tell the object to encode itself and save that record into the given key; we should end up recursing back into this function for each of the object's members

        let subCoder = new JSKeyedArchiver();
        if (typeof obj.encodeWith === "function") {
            // If the class has a defined encoder, then we'll use that
            obj.encodeWith(subCoder);
        } else if (obj.constructor.JSCODING_SYNTHESIZE_METHODS) {
            // Otherwise, if the class has opted into synthesizing an encoder, we'll introspectively read all the object's current properties and encode them
            try {
                for (let property of Object.keys(obj)) {
                    subCoder.encode(obj[property], property);
                }
            } catch (e) {
                throw new TypeError(`JSKeyedArchiver error: couldn't synthesize encodeWith(coder) for class ${obj.constructor.name}. Probably, one of its members doesn't confirm to JSCoding.`);
            }
        } else {
            throw new TypeError(`JSKeyedArchiver error: class ${obj.constructor.name} doesn't implement encodeWith(coder).`);
        }
        this.record[key] = subCoder.record;
    }

    encodedData() {
        return JSON.stringify(this.record);
    }
}


export class JSKeyedUnarchiver {
    constructor(record) {
        this.record = record;
    }


    static unarchivedObjectOfTypeFromData(type, data) {
        let coder = new JSKeyedUnarchiver(JSON.parse(data));
        return coder.decode(type, JSCODER_ROOT_OBJECT_KEY);
    }

    decode(type, key) {
        function getTypeOf(obj) {
            if (obj.constructor.name == "Array") {
                return new ArrayType(getTypeOf(obj[0]));
            } else {
                return obj.constructor;
            }
        }

        // Hey please don't request a key that doesn't exist, thanks in advance
        if (this.record[key] === undefined) {
            throw new Error(`JSKeyedUnarchiver error: unrecognized key ${key}`);
        }
        let subCoder = new JSKeyedUnarchiver(this.record[key]);

        // Recursive base case: if the request is for the raw data encoding of an object, then we'll just give the caller that thing directly
        if (type.name == "JSCoderEncodedData") {
            return new JSCoderEncodedData(subCoder.record);
        }

        // Special case check if the object is a bare Object to show a special error message; could've done this by having Object implement the JSCoding methods, but then literally every object of any type would inherit those.
        if (type.name === "Object") {
            throw new TypeError("JSKeyedUnarchiver error: bare Objects can't be decoded. If this is a struct-y object, consider declaring it as a class to provide type information for decoding.");
        }

        // Check if the requested key is for a boxed type
        try {
            return BoxedNothing.initWith(subCoder).unboxedNothing();
        } catch (e) {
            // Do nothing - proceed trying to decode with the type that was passed in
        }

        // Otherwise, tell the type to decode itself; we should end up recursing back into this function for each of the object's members
        if (typeof type.initWith === "function") {
            // If the class has a defined decoder, then we'll use that
            return type.initWith(subCoder);
        } else if (type.JSCODING_SYNTHESIZE_METHODS) {
            // Otherwise, if the class has opted into synthesizing a decoder, we'll construct a scratch instantiation of that class, introspectively read all of its properties and JSCODING_TYPE_ANNOTATIONS, and decode all those properties according to their detected or annotated types
            try {
                let args = type.JSCODING_CONSTRUCTOR_ARGUMENTS ?? [];
                let typeAnnotations = type.JSCODING_TYPE_ANNOTATIONS ?? {};
                let newObj = new type(...args);
                for (let property of Object.keys(newObj)) {
                    newObj[property] = subCoder.decode(typeAnnotations[property] ?? getTypeOf(newObj[property]), property);
                }
                return newObj;
            } catch (e) {
                throw new TypeError(`JSKeyedUnarchiver error: couldn't synthesize initWith(coder) for class ${type.name}. Probably, one of its members starts off undefined and its type couldn't be determined. Consider providing default constructor arguments or type annotations for the class.`);
            }
        } else {
            throw new TypeError(`JSKeyedUnarchiver error: class ${type.name} doesn't implement static initWith(coder).`);
        }
    }
}


// Utility function to get the type of an object for coding purposes. The reason you should use this instead of obj.constructor is that this will make an ArrayType object for you for arrays.
export function getTypeOf(obj) {
    let type;
    try {
        type = obj.constructor;
    } catch {
        return undefined;
    }

    if (type.name == "Array") {
        return new ArrayType(getTypeOf(obj[0]));
    } else {
        return type;
    }
}


// Arrays encoded & decoded with this must have a homogeneous element type, and you must provide that type when you try to decode an array. To do so, give an ArrayType object as the type argument instead of Array, like so:
//   let array = JSKeyedUnarchiver.unarchivedObjectOfTypeFromData(new ArrayType(Number), data);
//   coder.decode(new ArrayType(Number), "scores");
// ArrayType can be nested in itself if you have a multidimensional array.
const JSCODER_ARRAY_KEYS_KEY = "arraykeys";
export class ArrayType {
    constructor(baseType) {
        this.baseType = baseType;
    }

    initWith(coder) {
        let arrayKeys = JSON.parse(coder.decode(String, JSCODER_ARRAY_KEYS_KEY));

        let array = [];
        for (let key of arrayKeys) {
            array[key] = coder.decode(this.baseType, key);
        }
        return array;
    }
}


// This is a special, internal boxing type to enclose a "data representation" of an object so that it won't be encoded further.
// The intended use of this is to provide a base case for the coding recursion for "primitive" types, so that for example when a String hands its data to an archiver for encoding it isn't immediately kicked back into String.encodeWith again.
// Use of this should generally be avoided if possible in favor of composing primitives, which is why it's not exposed with an export. The actual data itself should be able to survive a round trip to and from JSON; strings are great.
class JSCoderEncodedData {
    constructor(data) {
        this.data = data;
    }
}

// This is a special, internal boxing type used to enclose undefined and null, which don't have constructors, so they can be serialized and unserialized.
// In JSKeyedArchiver, it's checked if this type can box a passed-in object before doing anything else, so it's important the constructor throws if it can't.
// In JSKeyedUnarchiver, it's checked if the object can be decoded into this type before doing anything else, so it's important that the static method throws if it can't AND it's important that we don't accidentally slurp up something that isn't one of these, so the encoding key is extra spicy here just to hopefully ensure we don't hit any accidental collisions. Like, if you create an object with a "jscoder_boxednothingtype" property then you're obviously doing it just to mess with me, right.
const JSCODER_BOXEDNOTHING_KEY = "jscoder_boxednothingtype";
class BoxedNothing {
    static nothingType = { undef: 0, nul: 1 };

    constructor(nothing) {
        if (nothing === undefined) {
            this.type = BoxedNothing.nothingType.undef;
        } else if (nothing === null) {
            this.type = BoxedNothing.nothingType.nul;
        } else {
            throw new TypeError("BoxedNothing: unrecognized nothing type");
        }
    }

    unboxedNothing() {
        if (this.type == BoxedNothing.nothingType.undef) {
            return undefined;
        } else if (this.type == BoxedNothing.nothingType.nul) {
            return null;
        } else {
            throw new TypeError(`Error unboxing nothing with unrecognized type ${this.type}`);
        }
    }

    encodeWith(coder) {
        coder.encode(this.type, JSCODER_BOXEDNOTHING_KEY);
    }

    static initWith(coder) {
        let type = coder.decode(Number, JSCODER_BOXEDNOTHING_KEY);
        let boxedNothing = new BoxedNothing();
        boxedNothing.type = type;
        return boxedNothing;
    }
}


// Extensions for built-in classes

const JSCODER_NUMBER_KEY = "number";
Number.prototype.encodeWith = function(coder) {
    coder.encode(new JSCoderEncodedData(this.toString()), JSCODER_NUMBER_KEY);
}
Number.initWith = function(coder) {
    return Number(coder.decode(JSCoderEncodedData, JSCODER_NUMBER_KEY).data);
}

const JSCODER_STRING_KEY = "string";
String.prototype.encodeWith = function(coder) {
    coder.encode(new JSCoderEncodedData(this), JSCODER_STRING_KEY);
}
String.initWith = function(coder) {
    return String(coder.decode(JSCoderEncodedData, JSCODER_STRING_KEY).data);
}

Array.prototype.encodeWith = function(coder) {
    let arrayKeys = [];
    for (let key of Object.keys(this)) {
        arrayKeys.push(key);
        coder.encode(this[key], key);
    }
    coder.encode(JSON.stringify(arrayKeys), JSCODER_ARRAY_KEYS_KEY);
}
Array.initWith = function(coder) {
    throw new TypeError("Decoding an Array requires specifying the type of its elements. To do this, provide an ArrayType object like `new ArrayType(<element type>)` instead of just Array.");
}

const JSCODER_BOOLEAN_KEY = "bool";
Boolean.prototype.encodeWith = function(coder) {
    coder.encode(this ? 1 : 0, JSCODER_BOOLEAN_KEY);
}
Boolean.initWith = function(coder) {
    return (coder.decode(Number, JSCODER_BOOLEAN_KEY) == 1);
}

const JSCODER_DATE_KEY = "date";
Date.prototype.encodeWith = function(coder) {
    coder.encode(this.toJSON(), JSCODER_DATE_KEY);
}
Date.initWith = function(coder) {
    return new Date(coder.decode(String, JSCODER_DATE_KEY));
}
